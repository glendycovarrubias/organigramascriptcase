/**
  	 * Para armar el organigrama
     *
     * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
     * date 2017-10-31
     * @version [1.0]
*/
// comprobar si tenemos los parametros puesto, nombrePuesto, dependenciaPuesto que viene dentro de la URL
if (isset($_GET["puesto"], $_GET["nombrePuesto"], $_GET["dependenciaPuesto"], $_GET["totalEmpleados"])) {
    //Recuperamos los valores de la seleccion de la lista de puestos
    $idPuesto  			= $_GET["puesto"]; 
	$nombrePuesto 		= $_GET["nombrePuesto"];
	$dependenciaPuesto 	= $_GET["dependenciaPuesto"];
	$totalEmpleados 	= $_GET["totalEmpleados"];
	
	/**
	 * Ejemplo:
	 * Mi seleccion		: ID: 2 | DESCRIPCION: GERENTE DE TESORERIA | DEPENDENCIA_ID: 138
	 * Busqueda 		: SELECT ID, DESCRIPCION, DEPENDENCIA_ID FROM PUESTOS WHERE DEPENDENCIA_ID=2
	 * Resultado		: [SUS HIJOS] (Resultado que trae los hijos puede llegar vacio [])
	 */

    //Hacemos una consulta para encontrar la dependencia_id que coincida con el $idPuesto de la seleccion
    $sqlPuestos = "SELECT ID, DESCRIPCION, DEPENDENCIA_ID FROM PUESTOS WHERE DEPENDENCIA_ID=$idPuesto";
    sc_lookup(datapuestohijos,$sqlPuestos);
	
    //Si el Resultado que trae los hijos es vacio o null entra a la condicion
	if({datapuestohijos} == null)
	{	
		//Esto significa que solo es hijo unico y no se comporta como padre	
		/**
		 * Como necesitamos conocer de quien depende o quien es su padre hacemos una consulta para averiguarlo por la cual tomamos la 
		 * $dependenciaPuesto y aremos la busqueda que coincida con el ID del puesto
		 */
		$sqlDependenciaPadre = "SELECT DESCRIPCION, DEPENDENCIA_ID FROM PUESTOS WHERE ID=$dependenciaPuesto";
		sc_lookup(datadependenciap,$sqlDependenciaPadre);		
		
		/*Aqui lo que hacemos es obtener el nombre del padre que nos arrojo la consulta $sqlDependenciaPadre hay que entrar a sus indices para recuperarlo*/
		$nombrePadre = {datadependenciap[0][0]};
		
			/*
			*Se realiza esto para que pueda visualizar completo el nombre del departamento ya que este es muy grande en algunos casos 				*entonces con una expresion regular captamos los espacion en blanco y les hago salto de linea ya que si impactamos los					*cambios del css en tamaños se defasa el organigrama
			*/
			$remplaceHijoNameEspBlanco 	= '<br>'; //Salto de linea
			$cadenaHijoName 			= $nombrePuesto; //Recuperamos el nombre de forma original del departamento
			$texHijo					= trim($cadenaHijoName);
			$expresionRegularHijoName 	= '/\s/'; //Expresion regular que ayuda a ver donde estan espacios en blanco
			$nameHijo					=  preg_replace($expresionRegularHijoName, $remplaceHijoNameEspBlanco, $texHijo);
		
		//Ahora formamos nuestro array para la seleccion que es solo hijo unico y no se comporta como padre
		$hijos[]=['idHijo'=>$idPuesto,'name'=>$nameHijo,'title'=>$totalEmpleados];
		
		/**
		 * Ejemplo
		 * Array convirtiendo a JSON : 
		 * [{idHijo: "1", name: "TRACKER", title: "17", relationship: "100", parentId: undefined}]
		 */
		
		//Y lo convertimos a JSON para poder pasarlo al javascript y pintar el organigrama
		$respuestaHijos = json_encode($hijos);
	}
	else
	{	
		/**
		 * Esto significa que si vino con varios hijos el Resultado que trae los hijos y que ademas se puede comportar como Padre como es 
		 * asi ya la consulta $sqlPuestos encontro a los hijos y los guardo es datapuestohijos se nos facilita poder obtener nuestra 
		 * logica
		 */
		
		/*Aqui lo que hacemos es obtener el nombre del padre que nos arrojo los valores de la seleccion de la lista de puestos*/
		$nombrePadre = $nombrePuesto;

		//Recorremos todo los hijos para poner los indices que necesitamos y lo depositamos en un array
		foreach ({datapuestohijos} as $valueHijos) {
			
			/*
			*Se realiza esto para que pueda visualizar completo el nombre del departamento ya que este es muy grande en algunos casos 				*entonces con una expresion regular captamos los espacion en blanco y les hago salto de linea ya que si impactamos los					*cambios del css en tamaños se defasa el organigrama
			*/
			$remplaceHijoNameEspBlanco 	= '<br>'; //Salto de linea
			$cadenaHijoName 			= $valueHijos[1]; //Recuperamos el nombre de forma original del departamento
			$texHijo					= trim($cadenaHijoName);
			$expresionRegularHijoName 	= '/\s/'; //Expresion regular que ayuda a ver donde estan espacios en blanco
			$nameHijo					=  preg_replace($expresionRegularHijoName, $remplaceHijoNameEspBlanco, $texHijo);

        	$hijos[] =['idHijo'=>$valueHijos[0],'name'=>$nameHijo,'title'=>$totalEmpleados];
    	}
		
    	/**
		 * Ejemplo
		 * Array convirtiendo a JSON : 
		 * [
		 * 	0:{idHijo: 17, 	name: "AUXILIAR VENTA DE SEGUNDA", 		title: 2, relationship: "110", parentId: undefined}
		 * 	1:{idHijo: 59, 	name: "RECEPCIONISTA", 					title: 2, relationship: "110", parentId: undefined}
		 * 	2:{idHijo: 329, name: "DILIGENCIERO DE CONTRALORIA", 	title: 2, relationship: "110", parentId: undefined}
		 * 	3:{idHijo: 374, name: "JEFE DE CORTE", 					title: 2, relationship: "110", parentId: undefined}
		 * 	4:{idHijo: 375, name: "JEFE DE SEGURIDAD E HIGIENE JR", title: 2, relationship: "110", parentId: undefined}
		 * 	]
		 */
		
		//Y lo convertimos a JSON para poder pasarlo al javascript y pintar el organigrama
    	$respuestaHijos = json_encode($hijos);
	}

	

?>
    <!-- Javascript que permite la funcionalidad del organigrama -->
    <script>
        $(document).ready(function(){

        		//Recuperamos el nombre del padre y el json
				var padre 		= <?php echo "'" . $nombrePadre . "'" ?>;
				var hijos 		= <?php echo $respuestaHijos ?>;
				
				var text = padre.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
				//Se realiza esto para poder mostrar el nombre completo con una expresion regular que ayude a dar un salto de linea
				var expresionRegular 	= /\s/g;
				var respuestaPadre		= text.replace(expresionRegular, '<br>');
			
				//Quitamos el organigrama para que al seleccionar no se quede con el organigrama anterior limpiamos rastros
                $('.orgchart').remove();
                
                //Objeto que lee el organigrama para formarlo
                var datascource = 
                    {
                        'name'		: respuestaPadre,
                        'title'		: '',
                        'children'	: hijos
                    };

                /**
                 * Este es la funcion de la libreria que lee el json o datasource y lo convierte en organigrama y le decimos que
                 * configuracion queremos para el organigrama
                 */
                var oc = $('#chart-containerBasico').orgchart({
                    'data'          		: datascource,
                    'nodeContent'   		: 'title',
					'pan'					: true,
      				'zoom'					: true,
                    'exportButton'  		: true,
                    'exportFilename'		: 'Organigrama',
					'exportFileextension' 	: 'pdf', //Con esta configuracion obtenemos la exportacion a PDF
                    'draggable'     		: true,
                    'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
                        if($draggedNode.find('.content').text().indexOf('manager') > -1 && $dropZone.find('.content').text().indexOf('engineer') > -1) {
                            return false;
                        }
                        return true;
                    }
                });

                oc.$chart.on('nodedropped.orgchart', function(event) {
                    console.log(
                        'draggedNode:' + event.draggedNode.children('.title').text()
                        + ', dragZone:' + event.dragZone.children('.title').text()
                        + ', dropZone:' + event.dropZone.children('.title').text()
                    );
                });
		
		    	oc.$chartContainer.on('touchmove', function(event) {
      				event.preventDefault();
    			});

		    	/* Para ocultar el conten del padre */
    			$('#chart-containerBasico > div > table > tr:nth-child(1) > td > div > div.content').css('display', 'none')

    			/* Refrescar el Organigrama */
                $('#btn-refrescarOrgBP').off().on('click', function(){
               		swal({
                        title : 'Cargando..',
                        text  : 'Espere unos segundos',
                        timer : 5000,
                        onOpen: () => {
                            swal.showLoading();
                        }
                    }).then((result) => {
						$('.orgchart').remove();
                   		oc.init({ 'data': datascource });
                        console.log('I was closed by the timer')
                	});     
                });

	            /* Guardar el Organigrama */
				$('#btn-guardarOrgBP').off().on('click', function(){
					var existeOrgLib = $('.orgchart').length;
					if (existeOrgLib){
						swal("Organigrama Guardado", "Click al boton!", "success"); 
					}else{
						swal(
							'Oops...',

							'No ha se ha detectado de la lista organigrama a generar!',

							'error'
						);
					}
				});

				/* Exportacion del Organigrama */
				$('#btn-imprimirOrgBP').off().on('click', function(){
					$(".oc-export-btn").click();
				});          
        });
    </script>
<?php
}else {
	echo "<p>No Existen los parametros</p>";
}
