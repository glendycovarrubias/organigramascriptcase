/**
     * Para armar el organigrama libre
     *
     * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
     * date 2017-12-05
     * @version [1.0]
*/

/* Sentencia SQL para traer la lista de Puestos */
    $sSQL = "SELECT ID, DESCRIPCION, DEPENDENCIA_ID FROM PUESTOS";
    sc_lookup(datapuesto,$sSQL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Organigrama Libre</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/orgchart@2.0.4/dist/css/jquery.orgchart.min.css">     
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <!-- 2 load the theme CSS file -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
    <!-- 4 include the jQuery library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
    <!-- 5 include the minified jstree source -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/jstree.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/orgchart@2.0.4/dist/js/jquery.orgchart.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
    <script type="text/javascript" src="https://unpkg.com/sweetalert2@7.0.8/dist/sweetalert2.all.js"></script>
    <style>
        /*Organigrama*/
        #chart-container { height: 600px; border: 2px solid #aaa; }
        .orgchart { background: #fff; }
        .orgchart .node .title 
        {
            text-align      : center;
            font-size       : 12px;
            font-weight     : 700;
            /*height          : 13em;*/
            /*line-height : 20px;*/
			height			: 100%;
            overflow        : hidden;
            text-overflow   : ellipsis;
            white-space     : nowrap;
            background-color: rgba(79, 166, 217, 0.8);
            color           : #fff;
            border-radius   : 4px 4px 0 0;
        }
        .orgchart .node .content {
            box-sizing      : border-box;
            width           : 100%;
            /*height: 20px;*/
            font-size       : 11px;
            /*line-height: 18px;*/
            border          : 1px solid rgba(79, 172, 217, 0.8);
            border-radius   : 0 0 4px 4px;
            text-align      : center;
            background-color: #fff;
            color           : #333;
            overflow        : hidden;
            text-overflow   : ellipsis;
            white-space     : nowrap;
        }

        .orgchart .node {
            display     : inline-block;
            position    : relative;
            margin      : 0;
            padding     : 3px;
            border      : 2px dashed transparent;
            text-align  : center;
			width		: 140px;
            /*width     : 500px;*/
            /*width       : 136px;*/
			/* width: 100%;*/
        }

        .orgchart .lines .leftLine {
            border-left  : 1px solid rgba(79, 172, 217, 0.8);
            float        : none;
            border-radius: 0;
        }

        .orgchart .lines .topLine {
            border-top: 2px solid rgba(79, 163, 217, 0.8);
        }

        .orgchart .lines .rightLine {
            border-right : 1px solid rgba(79, 149, 217, 0.8);
            float        : none;
            border-radius: 0;
        }

        .orgchart .lines .downLine {
            background-color: rgba(79, 172, 217, 0.8);
            margin          : 0 auto;
            height          : 20px;
            width           : 2px;
            float           : none;
        }
		
		/* Espacio botonera */
		div#botonera {
    		margin-top: 1em;
		}
		        
        /* Estilos de contenido scroll */
        div#container {
            height: 47em;
            overflow: scroll;
        }
		
        .tab-content {
            height: 43em;
			overflow: hidden;
			position: absolute;
            /*overflow: scroll;*/
        }
		
		div#contOrgLibre{
			margin-right: 0px;
    		margin-left: 0px;
			
		}

		/* Ocultando el boton de origen de la Exportación del organigrama */
		.oc-export-btn {
    		display: none;
		}
		
		/*Efecto del cargando loading*/
		.orgchart~.mask {
			position: fixed;
			top: 0;
			right: 0;
			bottom: 0;
			left: 0;
			z-index: 999;
			text-align: center;
			background-color: rgba(0,0,0,.3);
		}
		
		.orgchart~.mask .spinner {
			position: absolute;
			top: calc(50% - 54px);
			left: calc(56% - 54px);
			color: rgba(255,255,255,.8);
			font-size: 108px;
		}
    </style>
</head>
<body>
    <div id="contGralOrgLib" class="container-fluid">
        <div id="orgBP" class="row">
            <!-- Seccion de la lista Izquierda de los puestos -->
            <section id="sectionListPuestos" class="col-md-3">
                <!-- Para la lista -->
                <div id="container">
                    <div id="contenListDept">
                        <ul id="listDept">
                            <?php foreach({datapuesto} as $puestos){ 
								 $sSQLCont = "
												SELECT
												   count(puesto.ID) as totalEmp
												FROM PUESTOS AS puesto
												INNER JOIN EMPLEADO_LOG AS empleado
												ON puesto.ID = empleado.PUESTO_ID
												WHERE puesto.ID =".$puestos[0]." AND empleado.ACTIVO = 1
											";
                        
									sc_lookup(dataTotal, $sSQLCont);
									/* echo '<pre>';
									print_r({dataTotal});
									echo '</pre>';*/

									$totalEmp = {dataTotal};
							?>
                                <li id="<?php echo 'puesto'.$puestos[0] ?>" numEmp="<?php echo $totalEmp[0][0] ?>" text="<?php echo $puestos[1] ?>"><?php echo $puestos[1] ?></li>
                            <?php } ?> 
                        </ul>
                    </div>
                </div>
            </section>
            
            <!-- Seccion Botonera y contenedor del Organigrama -->
            <section id="sectionBotonera" class="col-md-9">
                <!-- contenedor de la botonera (Botones) -->
                <div id="botonera" class="btn-group" role="group" aria-label="...">
                    <!-- Generar el Organigrama -->
                    <button type="button" class="btn btn-primary" id="btn-generarOrgL" data-toggle="tooltip" title="Generar Organigrama">
                        <i class="fa fa-sitemap" aria-hidden="true"></i> Generar Organigrama
                    </button>
					<!-- Guardar -->
                    <button type="button" class="btn btn-success" id="btn-guardarOrgL" data-toggle="tooltip" title="Guardar">
                        <span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Guardar
                    </button>
					<!-- Refrescar -->
                    <button type="button" class="btn btn-default" id="btn-refrescarOrgL" data-toggle="tooltip" title="Refrescar">
                        <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Refrescar
                    </button>
                    <!-- Imprimir -->
                    <button type="button" class="btn btn-warning" id="btn-imprimirOrgL"  data-toggle="tooltip" title="Imprimir">
                        <i class="fa fa-print" aria-hidden="true"></i> Imprimir
                    </button>
                </div>
                <!-- contenedor para generar el organigrama -->
                <div class="tab-content">
                    <!-- Para el organigrama -->
                    <div id="contOrgLibre" class="row"></div>
                </div>
            </section>
        </div>
    </div>
    <script>
        $( document ).ready(function() {
            /* Funcionalida para la lista */
            $('#contenListDept').jstree({
                "types"     : { "default" : { "icon" : "fa fa-building" }, "demo" : { "icon" : "fa fa-building" } },
                'core'      : {'check_callback'   :   true},
                "plugins"   : [ "unique", "dnd", "types" ]
            });
            /* Fin de la funcionalidad de la lista */

            /* Funcionalidad del boton para generar el organigrama */
            $('#btn-generarOrgL').on('click', function(){
                var existente = $('.jstree-closed').length || $('.jstree-open').length;
                if(existente){    
    				/* Colocamos la opción que nos permite abrir todos los nodos antes de que se crea el organigrama 
    				 * si en caso dado al usuario deja un nodo cerrado
    				*/
				    $("#contenListDept").jstree('open_all'); 
                    /* Plugin alert swift */
    				    swal({
                                title : 'Cargando..',
                                text  : 'Espere unos segundos',
                                timer : 5000,
                                onOpen: () => {
                                    swal.showLoading()
                                }
                        }).then((result) => {
                            $('.orgchart').remove(); //Removemos el organigrama anterior dibujado
            
                            $('#contOrgLibre').orgchart({
                                'data'          		: $('.jstree-container-ul'),
								'nodeContent'   		: 'title',
    							'pan'					: true,
      							'zoom'					: true,
    							'exportButton'  		: true,
    							'exportFilename'		: 'Organigrama',
    							'exportFileextension' 	: 'pdf', //Con esta configuracion obtenemos la exportacion a PDF
								'draggable'     		: true, //Arrastrar Organigrama
								/* 'initCompleted': function($chart) {} , sirve para un callback */
                            });

                    
                            var arrayTitle = {
                                idPuesto    	: [],
                                textPuesto  	: [],
								numEmpleados 	: []
                            };

                            $(".jstree-container-ul li").each(function(){
                                var idPuestoDom         = $(this).attr('id');
                                var titlePuestoDom      = $(this).attr('text');
								var numEmpleadosDom		= $(this).attr('numEmp');
                                arrayTitle.idPuesto.push(idPuestoDom);
                                arrayTitle.textPuesto.push(titlePuestoDom);
								arrayTitle.numEmpleados.push(numEmpleadosDom);
                            });
		
                            var totalPuesto = arrayTitle.idPuesto.length;
                            for (var i = 0; i < totalPuesto; i++){
                                $("#"+arrayTitle.idPuesto[i]+" .title").text(arrayTitle.textPuesto[i]);//Colocamos nombre de puesto
										
								/* Extraemos el nombre y aplicamos expresion regular para dar salto de liena en espacio blanco */
								var titleDomm			= $("#"+arrayTitle.idPuesto[i]+" .title").text();
								var text 				= titleDomm.replace(/([\ \t]+(?=[\ \t])|^\s+|\s+$)/g, '');
								var expresionRegular 	= /\s/g ;
								var respuestaPadre		= text.replace(expresionRegular, '<br>');
								
								$("#"+arrayTitle.idPuesto[i]+" .title").html(respuestaPadre); //Agregamos el nuevo nombre con espacio
								$("#"+arrayTitle.idPuesto[i]+" .content").text(arrayTitle.numEmpleados[i]);//Colocamos numero de Empleados
                            }																	
                        });
                    /* Fin de la funcionalidad del alert swift */
				}else{
					swal(
						  'Oops...',
						  'No ha se ha detectado de la lista organigrama a generar!',
						  'error'
					);
				}
            });
            /* Fin de Funcionalida para Organigrama */
                            

            /*Refresca el contenido del organigrama */
            $('#btn-refrescarOrgL').on('click', function(){
                $('.orgchart').remove(); //Eliminamos el organigrama 
                
                $('#contenListDept').jstree("refresh"); // Reiniciamos el plugin de la lista de depto
                
                /* Plugin alert swift */
                swal({
                        title : 'Cargando..',
                        text  : 'Espere unos segundos',
                        timer : 5000,
                        onOpen: () => {
                            swal.showLoading();
                        }
                    }).then((result) => {
                        // if (result.dismiss === 'timer') {
                        console.log('I was closed by the timer')
                        // }
                });     
            });  
            /* Fin de Funcionalida para refrescar el Organigrama */

            /* Funcionalidad para guardar */
            $('#btn-guardarOrgL').on('click', function(){
				var existeOrgLib = $('.orgchart').length;
				if (existeOrgLib){
					swal("Organigrama Guardado", "Click al boton!", "success"); /* Plugin alert swift */
				}else{
					swal(
						  'Oops...',
						  'No ha se ha detectado de la lista organigrama a generar!',
						  'error'
					);
				}
            });
            /* Fin de Funcionalida para guardar el Organigrama */
			
			/* Funcionalidad para la exportación de la imagen del organigrama */
			$('#btn-imprimirOrgL').on('click', function(){
				var existeOrgLib = $('.orgchart').length;
				if (existeOrgLib){
					$(".oc-export-btn").click();
				}else{
					swal(
						  'Oops...',
						  'No ha se ha detectado de la lista organigrama a generar!',
						  'error'
						);
				}
			});
            /* Fin de Funcionalidad para la exportación de la imagen del organigrama */
        });
    </script>
</body>
</html>
<?php