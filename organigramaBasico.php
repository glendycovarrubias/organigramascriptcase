/**
     * Para armar el organigrama
     *
     * @author   Glendy Covarrubias <glendycovarrubias@hotmail.com>
     * date 2017-10-31
     * @version [1.0]
*/
$sSQL = "SELECT ID, DESCRIPCION, DEPENDENCIA_ID FROM PUESTOS";
sc_lookup(datapuesto,$sSQL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Organigrama</title>
    <!-- Librerias externas lo mejor es bajar las librerias al proyecto -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/orgchart@2.0.4/dist/css/jquery.orgchart.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="http://code.jquery.com/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/orgchart@2.0.4/dist/js/jquery.orgchart.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    
    <!-- Optional estilos de tablas  -->
    <link rel="stylesheet" href="https://rawgit.com/wenzhixin/bootstrap-table/master/src/bootstrap-table.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified JavaScript  tablas-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    <!-- SweatAlert -->
    <script type="text/javascript" src="https://unpkg.com/sweetalert2@7.0.8/dist/sweetalert2.all.js"></script>
    
    <!-- Estilos del proyecto es mejor ponerlo en documento aparte y jalarlo -->
    <style>
		/*Contenedor de la vista de esta seccion*/
		div#orgBP {
    		margin-top: 1em;
		}
		
		/*Botones de guardar y refrescar*/
		.marginbtn{
			float: right;
    		margin-left: 1em;
			//margin-right: 5em;
		}
		
		/*Buscador y lista de Puestos*/
		div#cont-tbPuesto {
    		height: 41em;
    		margin-top: 1em;
    		overflow-x: hidden;
    		overflow-y: auto;
		}
		
        /*Organigrama*/
        #chart-containerBasico { width: 71em; height: 580px; border: 1px solid #aaa; margin-left: 0px; }
        .orgchart { background: #fff; }
        .orgchart .node .title 
        {
            text-align      : center;
            font-size       : 12px;
            font-weight     : 700;
            /*height          : 13em;*/
            /*line-height : 20px;*/
			height			: 100%;
            overflow        : hidden;
            text-overflow   : ellipsis;
            white-space     : nowrap;
            background-color: rgba(79, 166, 217, 0.8);
            color           : #fff;
            border-radius   : 4px 4px 0 0;
        }
		
        .orgchart .node .content {
            box-sizing      : border-box;
            width           : 100%;
            /*height: 20px;*/
            font-size       : 11px;
            /*line-height: 18px;*/
            border          : 1px solid rgba(79, 172, 217, 0.8);
            border-radius   : 0 0 4px 4px;
            text-align      : center;
            background-color: #fff;
            color           : #333;
            overflow        : hidden;
            text-overflow   : ellipsis;
            white-space     : nowrap;
        }

        .orgchart .node {
            display     : inline-block;
            position    : relative;
            margin      : 0;
            padding     : 3px;
            border      : 2px dashed transparent;
            text-align  : center;
			width		: 140px;
            /*width     : 500px;*/
            /*width       : 136px;*/
			/* width: 100%;*/
        }

        .orgchart .lines .leftLine {
            border-left  : 1px solid rgba(79, 172, 217, 0.8);
            float        : none;
            border-radius: 0;
        }

        .orgchart .lines .topLine {
            border-top: 2px solid rgba(79, 163, 217, 0.8);
        }

        .orgchart .lines .rightLine {
            border-right : 1px solid rgba(79, 149, 217, 0.8);
            float        : none;
            border-radius: 0;
        }

        .orgchart .lines .downLine {
            background-color: rgba(79, 172, 217, 0.8);
            margin          : 0 auto;
            height          : 20px;
            width           : 2px;
            float           : none;
        }
		
		//Icono
		.orgchart .node .title .symbol {
			float: left;
			margin-top: 4px;
			margin-left: 2px;
			display: none;
		}
		        
        /* Boostrab tabs */
        .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
            color: #464a4c;
            background-color: #fff;
            border-color: #aaa #aaa #fff;
        }
        
        .nav-tabs .nav-link {
            border: 1px solid transparent;
            border-top-right-radius: .25rem;
            border-top-left-radius: .25rem;
        }
        
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
            color: #555;
            cursor: default;
            background-color: #fff;
            border: 1px solid #aaa;
            border-bottom-color: transparent;
        }
        
        .nav-link {
            display: block;
            padding: .5em 1em;
        }
        /* Fin de Boostrap tabs */ 
		
		/* Ocultando el boton de origen de la Exportación del organigrama */
		.oc-export-btn {
    		display: none;
		}
    </style>
    
    <!-- Javascript del proecyto es mejor ponerlo en documento aparte y jalarlo -->
    <script type="text/javascript">
        $(document).ready(function () {
            (function ($) {
                //Esta funcion sirve para hacer la busqueda de la lista de puestos
                $('#filtrar').keyup(function () {
                    var rex = new RegExp($(this).val(), 'i');
                    $('.buscar tr').hide();
                    $('.buscar tr').filter(function () {
                        return rex.test($(this).text());
                    }).show();
                })
            }(jQuery));
        });
    </script>    
</head>
<body>
    <div class="container-fluid">
        <div id="orgBP" class="row">
            <!-- Seccion de la lista -->
            <section class="col-md-3">
                <!-- Contenedor del buscador -->
                <div class="input-group">
					<span class="input-group-addon">
						<span class="glyphicon glyphicon-search"></span>
					</span>
                    <input id="filtrar" type="text" class="form-control" placeholder="Buscador...">
                </div>
                <!-- Contenedor de la lista (tabla)-->
				<div id="cont-tbPuesto">
                <table data-toggle="table" class="table table-hover">
                    <thead>
                      <tr>
                        <th>Puestos</th>
                      </tr>
                    </thead>
                    <tbody class="buscar">
                       <?php foreach({datapuesto} as $puestos){
							
							$sSQLCont = "
											SELECT
											   count(puesto.ID) as totalEmp
											FROM PUESTOS AS puesto
											INNER JOIN EMPLEADO_LOG AS empleado
											ON puesto.ID = empleado.PUESTO_ID
											WHERE puesto.ID =".$puestos[0]." AND empleado.ACTIVO = 1
										";
						
							sc_lookup(dataTotal, $sSQLCont);
							/* echo '<pre>';
							print_r({dataTotal});
							echo '</pre>';*/
							
							$totalEmp = {dataTotal};
							
					    ?>
                            <tr>
                                <td id="<?php echo $puestos[0] ?>" data-name="<?php echo $puestos[1] ?>" data-dependencia="<?php echo $puestos[2] ?>" numEmp="<?php echo $totalEmp[0][0] ?>" class="listOrganigrama"><?php echo $puestos[1] ?></td>
                            </tr>
                       <?php } ?>
                    </tbody>
                </table>
				</div>
            </section>
            <!-- Seccion del organigrama -->
            <section class="col-md-9">
                <div id="botonera" class="row">
                    <div class="col-md-8">
                        <ul class="nav nav-tabs" role="tablist">
                          <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#chart-containerBasico" role="tab">Basico</a>
                          </li>
                          <!--<li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#chart-containerPersonalizado" role="tab">Personalizado</a>
                          </li>-->
                        </ul>
                    </div>
                    <div class="col-md-4">
						<div id="botonera" class="btn-group" role="group" aria-label="...">
							<!-- Guardar -->
							<button type="button" class="btn btn-success" id="btn-guardarOrgBP" data-toggle="tooltip" title="Guardar">
								<span class="glyphicon glyphicon-saved" aria-hidden="true"></span> Guardar
							</button>
							<!-- Refrescar -->
							<button type="button" class="btn btn-default" id="btn-refrescarOrgBP" data-toggle="tooltip" title="Refrescar">
								<span class="glyphicon glyphicon-refresh" aria-hidden="true"></span> Refrescar
							</button>
							<!-- Imprimir -->
							<button type="button" class="btn btn-warning" id="btn-imprimirOrgBP"  data-toggle="tooltip" title="Imprimir">
								<i class="fa fa-print" aria-hidden="true"></i> Imprimir
							</button>
						</div>
                    </div>
                </div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="chart-containerBasico" class="row tab-pane fade in active" role="tabpanel"></div>
                    <div id="chart-containerPersonalizado" class="tab-pane" role="tabpanel">... <h1>Personalizado</h1>...</div>
                </div>
            </section>
        </div>
    </div>

    <!-- Javascript que permite la funcionalidad del organigrama -->
    <script>
        $(document).ready(function(){
            //Evento que escucha el click de las opciones de la lista que fue seleccionada
            $(".listOrganigrama").on('click', function(){
                var seleccion           = $(this).attr('id');
                var nombrePuesto        = $(this).data('name');
                var dependenciaPuesto   = $(this).data('dependencia');
				var totalEmpleados		= $(this).attr('numEmp');
				console.log(totalEmpleados);
                //Hay que buscar como cambiar esta ruta de manera correcta
				var ruta = "http://127.0.0.1:8090/scriptcase/app/organigramas/consultaorganigrama/?nmgp_outra_jan=true&nmgp_start=SC&script_case_session=c3u7sk7s9hqav3jhjenfunqf44&8692";

                $("#chart-containerBasico").load(ruta+$.param(
                    {numarticulos: ' ',puesto: seleccion,nombrePuesto: nombrePuesto, dependenciaPuesto: dependenciaPuesto, totalEmpleados: totalEmpleados}
                ));
            });
        });
    </script>
</body>
</html>
<?php
